import  {gql} from '@apollo/client';

export const REGISTRAR_USUARIO_NEW = gql`

mutation ($usuario: UsuarioInput!) {
    usuarios {
      registrarUsuario(usuario: $usuario) {
        nombre
        user
        correo
        contra
        direccion
        rol
        telefono
      }
    }
  }
  
`;

export const ACTUALIZAR_USUARIO = gql`

mutation($usuario: UsuarioInput!){
  usuarios {
    registrarUsuario(usuario:$usuario){
      id
      correo
      nombre
      rol
      direccion
      telefono
      user
      
    }
  }
}
  
`;

export const RECUPERAR_CONTRA  = gql`

mutation($id:ID!,$password:String!){
  usuarios{
    resetPassword(id:$id,password:$password){
   		id
      contra
      nombre
      user
 
    }
  }
}
  
`;


