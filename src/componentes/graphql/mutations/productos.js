import { gql } from '@apollo/client';

export const PRODUCTO_FRAGMENT = gql`
  fragment ProductoFragment on Producto {
    id
    nombre
    precio
    cantidad
    categoria
    descripcion
    dateCreated
    isActive
    isFavorit
  }
`;
export const AGREGAR_PRODUCTO = gql` 
mutation($producto:ProductoInput!){
    productos{
      registrarProducto(producto:$producto){
        ...ProductoFragment
      }
    }
  }
  ${PRODUCTO_FRAGMENT}
  `;

  export const CAMBIAR_FAVORITO = gql` 
  mutation($id:ID!, $favorit:Boolean!){
    productos{
      cambiarEstusFav(id:$id,favorit:$favorit){
         ...ProductoFragment
      }
    }
  }
  ${PRODUCTO_FRAGMENT}
  `;

export const CAMBIAR_ESTATUS = gql` 
 mutation($id:ID!){
    productos{
      cambiarEstatus(id:$id){
        ...ProductoFragment
      }
    }
  }
  ${PRODUCTO_FRAGMENT}
  `;