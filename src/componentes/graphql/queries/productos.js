import { gql } from '@apollo/client';

export const PRODUCTO_FRAGMENT = gql`
  fragment ProductoFragment on Producto {
    id
    nombre
    precio
    cantidad
    categoria
    descripcion
    dateCreated
    isActive
    isFavorit

  }
`;
export const OBTENER_PRODUCTOS = gql`
  query{
    productos {
      obtenerTodos{
        ...ProductoFragment
        
      }
    }
  }
  ${PRODUCTO_FRAGMENT}
`;

export const OBTENER_PRODUCTO_POR_ID = gql`
  query($id: ID!) {
    productos {
      obtenerById(id: $id) {
        ...ProductoFragment
                                                                                                                          
      }
    }
  }
  ${PRODUCTO_FRAGMENT}
`;


export const OBTENER_PRODUCTO_POR_NOMBRE = gql`
  query($nombre: String!) {
    productos {
      obtenerByName(nombre: $nombre) {
        ...ProductoFragment
        
      }
    }
  }
  ${PRODUCTO_FRAGMENT}
`;
export const OBTENER_PRODUCTOS_POR_CATEGORIA = gql`
  query($categoria: String!) {
    productos {
      obtenerByCategory(categoria:$categoria) {
        ...ProductoFragment
      }
    }
  }
  ${PRODUCTO_FRAGMENT}
`;
