import {gql} from '@apollo/client';

export const USUARIO_FRAGMENT = gql`
  fragment UsuarioFragment on Usuario{
    id
    nombre
    user
    direccion
    telefono
    rol
   
  } 
`;  

export const Login_Usuario = gql`
query ($user: String!, $contra: String!) {
  usuarios {
    loginUser(user: $user,contra: $contra) {
   
    ...UsuarioFragment


    }
  }
}
${USUARIO_FRAGMENT}
`;


export const DATOS_USUARRIO_BYID = gql`
 query($id:ID!){
  usuarios{
    obtenerById(id:$id){
     nombre
      user
      correo
      direccion
      telefono
      rol
      id
    }
  }
}
`;

export const USUARIO_BY_CORREO_TELEFONO = gql`
query($correo:String!,$telefono:String!){
  usuarios{
    buscarUser(correo:$correo,telefono:$telefono){
      id
      nombre
      correo 
      direccion
      telefono
      rol 
    }
  }
}
`;

export const BUSCAR_POR_NOMBRE = gql`
query($user:String!){
  usuarios{
    obtenerPorUsuario(usuario:$user){
      nombre
      user
    }
  }
}
`;