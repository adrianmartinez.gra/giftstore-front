import React,{useState} from 'react';
import './miPerfil.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCoffee} from '@fortawesome/free-solid-svg-icons'
import {faUser, faUserEdit, faPlusSquare,faArrowAltCircleUp} from '@fortawesome/free-solid-svg-icons';
import sticker from '../IMG/scotch.png';
import {Link} from 'react-router-dom';
 
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {useMutation, useQuery} from '@apollo/client';
import swal from 'sweetalert';
import history from '../utils/history';
import {ACTUALIZAR_USUARIO} from '../graphql/mutations/usuario';
import { DATOS_USUARRIO_BYID } from '../graphql/queries/usuario';
import {BUSCAR_POR_NOMBRE} from '../graphql/queries/usuario';
import firebase from 'firebase'
import storage from 'firebase'
const MiPerfil = (props) => {


//Mutacion
const [updateTodo] = useMutation(ACTUALIZAR_USUARIO,{
    onCompleted:(result => {
        swal({title: "Se Actualizo Correctamente", icon: "success", button: "Aceptar!"});
        //setDatos(localStorage.setItem('datosUsuario',JSON.stringify(result.usuarios.registrarUsuario)))
        console.log(result.usuarios.registrarUsuario)
        setdatosUsuario(result.usuarios.registrarUsuario)
        refetchUser1()
        toggle()
        
    })
   
});

const refetchUser1 = () =>{
    console.log("actualizado")
    refetchUser()
}


const [datosUsuario, setdatosUsuario] = useState(null)

const {data,refetch:refetchUser} = useQuery(DATOS_USUARRIO_BYID,{
    variables:{
        id: localStorage.getItem('datosUsuario')
    
    },
     onCompleted:(res => {
         setdatosUsuario(res.usuarios.obtenerById)
         console.log(res.usuarios.obtenerById)
     })
})


const [userName, setuserName] = useState("")
const {loadding,error} = useQuery(BUSCAR_POR_NOMBRE,{
    variables:{
        user:userName
    },
    onCompleted:(res =>{
        if(res.usuarios.obtenerPorUsuario == null){  
        console.log(res)
        }else{ 
        swal({title: "error", text: "Ese usuario ya existe",icon: "error", button: "Ok"})
       
        }
    })
})


//modal
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);

const [usuario,setFormulario] = useState(null) 
    // const [datos,setDatos] = useState(localStorage.getItem(JSON.stringify('datosUsuario'))) 
    //Metodo Actualizar
    const Actualizar = (e) =>{
        updateTodo({
            variables:{
                usuario:{...usuario,id:datosUsuario.id} 
            } 
        })
        console.log(usuario)
    }


    const [foto, setfoto] = useState("")
    const handleOnChangeFoto = (event) =>{
        const image = event.currentTarget.files[0]
        const storageRef = storage().ref(`/FotosUsuarios/${image.name}`).put(image);
       
        storageRef.on('state-changed', snapshot =>{ },
        ()=>{
            storage.ref("FotosUsuarios").child(image.name).getDownloadURL().then(url => {
                console.log(url)
            })
        }
        
        )
    }


    if (datosUsuario) {
        return(
            <div className="container">
            <div>
                 
               <div>

                    <span>
                        <img src={sticker}
                            alt="sticker"
                            id="sticker1"/>
                        <img src={sticker}
                            alt="sticker"
                            id="sticker2"/>
                    </span><br/>
                    <div className="clearfix"></div>
                    <div className="MiUsuario">
                        {/* <span  id="InputFileFoto"> */}

                        <img src={foto} alt="fotoUsuario" width="220"/>

                        <Button id="ButonFile"  >
                            <FontAwesomeIcon id="iconoUpload" icon={faArrowAltCircleUp}/>
                        <p id="textoUploadFile">Upload a file</p>
                        </Button> 
                              <input type="file" id="estiloInputFIle" 
                              
                               onChange={ event => handleOnChangeFoto(event)}/> 
                            {/* </span> */}


                         <div className="clearfix"></div>
                        <div>
                            <FontAwesomeIcon icon={faUser}
                                id="iconUser"/>
                                <p id="usuarioname">
                                {  datosUsuario ? datosUsuario.nombre : ""  }
                                 </p>
                        </div>
                        <div className="clearfix"></div>
                       < div id="CorreoUsuarioFlotante" ><p>{datosUsuario ? datosUsuario.correo : ""}</p></div>
                        <hr/>
                        <div>
                        { datosUsuario.rol=='Vendedor'   ?  
                            <Link to="/ListadoProductos">
                                <button className="btn btn-primary" id="btn-AddProducto">
                                    <p id="addProdTexto">Administrar Productos</p>
                                    <FontAwesomeIcon icon={faPlusSquare}
                                        id="addProdIcon"/>
                                </button>
                            </Link>
                        : ""} 
                        </div>

                    </div>
                 </div>

                 
              </div>
                <div className="MiPerfil" >
                    <span> {/* <img src={sticker} alt="sticker" id="sticker3"/>
                     <img src={sticker} alt="sticker" id="sticker4"/> */} </span><br/>
                    <div id="titulo">
                        <h2>Mi Perfil</h2>
                    </div>
                    <div id="btnActualizar">
                        <button onClick={toggle} className="btn btn-primary">
                            Editar
                            <FontAwesomeIcon icon={faUserEdit}
                                id="UserEditI"/>
                        </button>
                    </div>
                    <br/>
                    <div>
                        <hr/>

                        <form  >
                         
                            <label  id="label">Nombre</label> 
                            <input type="text" className="form-control" id="tmñoMiPerfInpu"
                                defaultValue={
                                    datosUsuario ? datosUsuario.nombre : ""
                                }
                                disabled/>

                            <label  id="label">Usuario
                            </label>
                            <input type="text" className="form-control" id="tmñoMiPerfInpu"
                                value={
                                    datosUsuario ? datosUsuario.user : ""
                                }
                                disabled/>

                                <label  id="label">Correo
                            </label>
                            <input type="Email" className="form-control" id="tmñoMiPerfInpu"
                                value={
                                    datosUsuario ? datosUsuario.correo : ""
                                }
                                disabled/>

                            <label  id="label">Direccion
                            </label>
                            <input type="text" className="form-control" id="tmñoMiPerfInpu"
                                value={
                                    datosUsuario ? datosUsuario.direccion : ""
                                }
                                disabled/>

                            <label  id="label">Telefono
                            </label>
                            <input type="text" className="form-control" id="tmñoMiPerfInpu"
                                value={
                                    datosUsuario ? datosUsuario.telefono : ""
                                }
                                disabled/>

                            <label  id="label">Rol
                            </label>
                            <input type="text" className="form-control" id="tmñoMiPerfInpu"
                                value={
                                    datosUsuario ? datosUsuario.rol : ""
                                }
                                disabled/>

                        </form>
                         

                    </div>
             </div>
              {/* : ""  } */}
              <br/>

              <div>
                   <Modal isOpen={modal} toggle={toggle}  >
                    <ModalHeader id="modal-title"><p id="titleModalUpdatePerfil" >  Actualizar Perfil</p></ModalHeader>
                      <form onSubmit={ e =>{
                          e.preventDefault();
                          Actualizar()
                      }}>
                      <ModalBody id="contentbodyModalMiperfil">
                    
                          {/* <label id="labelId">Id:</label> <input type="text" className="form-control" id="tmñoMiPerfInpu" value={
                                    datos ? datoss[0].id : ""
                                }  disabled/> */}
                                <div className="clearfix"></div>

                            <label id="label">Nombre</label>
                            <input type="text" className="form-control" id="tmñoMiPerfInpu"
                            defaultValue={datosUsuario.nombre}
                                  onChange={  e => {
                                        setFormulario({...usuario, nombre: e.currentTarget.value})
    
                                       }
                                    }
                              required  />

                            <label id="label">Usuario
                            </label>
                            <input type="text" className="form-control" id="tmñoMiPerfInpu"
                             defaultValue={datosUsuario.user}
                                   onChange={  e => {
                                        setFormulario({...usuario, user: e.currentTarget.value})
                                       setuserName( e.currentTarget.value)

    
                                       }

                                    }
                                    required   />

                                
                            <label id="label">Correo
                            </label>
                            <input type="text" className="form-control" id="tmñoMiPerfInpu"
                             defaultValue={datosUsuario.correo}
                                   onChange={  e => {
                                        setFormulario({...usuario, correo: e.currentTarget.value})
    
                                       }
                                    }
                                    required   />

                            <label id="label">Direccion
                            </label>
                            <input type="text" className="form-control" id="tmñoMiPerfInpu"
                             defaultValue={datosUsuario.direccion}
                                   onChange={  e => {
                                        setFormulario({...usuario, direccion: e.currentTarget.value})
    
                                       }
                                    }
                                    required    />

                            <label id="label">Telefono
                            </label>
                            <input type="text" className="form-control" id="tmñoMiPerfInpu"
                             defaultValue={datosUsuario.telefono}
                              onChange={  e => {
                                        setFormulario({...usuario, telefono: e.currentTarget.value})
    
                                       }
                                    }
                                    minLength="10"
                                    required  /> <br/>

                           <div className="form-group" id="tmñoMiPerfInpu"> <select name="Cliente" className="form-control" 
                                    defaultValue={datosUsuario.rol}
                                   onChange={  e => {
                                        setFormulario({...usuario, rol: e.currentTarget.value})
    
                                       }
                                    }
                                    required    >
                                    <option selected>Rol</option>
                                    <option name="Vendedor" value="Vendedor">Vendedor</option>
                                    <option name="Cliente" value="Cliente">Cliente</option>
    
                                </select>
                                </div>
                                <hr/>
                            <div id="footerButtonEditar">
                                   <Button color="primary" type="submit">Guardar</Button>{' '}
                                <Button color="secondary" onClick={toggle}>Cancel</Button>
                            </div>
                            <br/>
                              
                        </ModalBody>
                         </form>
                 </Modal>
            </div>
        </div>

            );
    }
    else{return <p></p> }
    
}

export default MiPerfil;
