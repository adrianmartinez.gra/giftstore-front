import React, {useState} from 'react';
import './recuperarContra.css';
import swal from 'sweetalert';
import {USUARIO_BY_CORREO_TELEFONO} from '../graphql/queries/usuario';
import {RECUPERAR_CONTRA} from '../graphql/mutations/usuario';

import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter
} from 'reactstrap';

import {useLazyQuery, useMutation} from '@apollo/client';
import history from '../utils/history';


const ResetPassword = (props) => { // const {buttonLabel, className} = props;


    const [modal, setmodal] = useState(false)
    const toggle = () => setmodal(!modal);

    const [datosUsuario, setdatosUsuario] = useState(null)

    const [buscarUsuario] = useLazyQuery(USUARIO_BY_CORREO_TELEFONO, {
        onCompleted: (result => {
                if (result.usuarios.buscarUser != null) {
                        try {
                            setdatosUsuario(result.usuarios.buscarUser)
                            console.log(result.usuarios)
                            
                            setmodal(true)
                        } catch {
                            swal(
                                {title: "Error", text: "No se encontro el usuario", icon: "error"}
                            );

                        }
                    } else  { 
                            // if(result == null)
                            return   swal({title: "Error", text: "No se encontro el usuario", icon: "error"})
                    }
                }
            )}
    )
    const [RestablecerPwd] = useMutation(RECUPERAR_CONTRA, {
        onCompleted: (resp => {
            if (resp.usuarios.ResetPassword != null) {
                swal({title: "Operacion exitosa!", text: "La contraseña se actualizo correctamente", icon: "success"})
                console.log(resp)
                setmodal(false)
            }
            swal({title: "Ho ho!", text: "Error al reestablecer su contraseña", icon: "error"})
            
        })
    })


    const [updatePwd, setupdatePwd] = useState(null)
    const [confirmPwd, setconfirmPwd] = useState(null)

    const RestorePassword = () => {
        if (confirmPwd != updatePwd) 
            return swal({title: "Error", text: "Las contraseñas no coinciden", icon: "error"})
        
        RestablecerPwd({
            variables: {
                password: updatePwd,
                id: datosUsuario.id
            }
        })

    }

    const [correo, setCorreo] = useState(false);
const [telefono, setTelefono] = useState(false);

const restablecerContraseña = () => {
    if(correo ==""|| telefono =="")  return swal({title: "Error", text: "los campos son requeridos", icon: "error"})

    buscarUsuario({
        variables: {
            correo: correo,
            telefono: telefono
        }
    })
}

return (
    <div className="container" id="contenedorgeneral">
        <div className="card-header">
            <h1>
                Reestablecer Contraseña</h1>
        </div>
        <div className="card-body">
            <form id="ContentFormulario"
                /*onSubmit={e=> { e.preventDefault()
                    restablecerContraseña()
                }}*/
            >

                <div className="form-group">
                    <label htmlFor="">
                        Correo

                    </label>
                    <input type="text" className="form-control" id="inputRestablecerPass"
                        onChange={
                            (e) => {
                                setCorreo(e.currentTarget.value)
                            }
                        }
                        placeholder="Nombre de usuario.."/>

                </div>


            <div className="form-group">
                <label>
                    Telefono

                </label>
                <input type="number" className="form-control" id="inputRestablecerPass"
                    onChange={
                        (e) => {
                            setTelefono(e.currentTarget.value)
                        }
                    }
                    placeholder="ejemp: 64412523..."
                    required/>

            </div>
        <hr/>


        <div className="clearfix"></div>

        <div className="form-group">
            <Button color="success"
                /*onClick={toggle}*/
                id="btnRestablecer"
                onClick={
                    (e) => restablecerContraseña(e)
            }>
                Reestablecer</Button>
            {/* <button  className="btn btn-success" id="btnRestablecer">Reestablecer</button> */} </div>


    </form>

</div>


<div>
    <Modal isOpen={modal}
        /*     toggle={toggle}*/
    >
        <ModalHeader
            /*toggle={toggle}*/
        >
            Nueva Contraseña</ModalHeader>
        <ModalBody>
            <input type="password" className="form-control" placeholder="Nueva Contraseña"
                onChange={
                    e => {
                        setupdatePwd(e.currentTarget.value)
                    }
                }/>
            <br/>
            <input type="password" className="form-control" placeholder="Confirmar Contraseña"
                onChange={
                    e => {
                        setconfirmPwd(e.currentTarget.value)
                    }
                }/>

        </ModalBody>
    <ModalFooter>
        <Button color="primary"
            onClick={
                e => RestorePassword()
        }>Guardar</Button>
        {' '}
        <Button color="secondary"
            onClick={
                () => setmodal(false)
        }>Cancel</Button>
    </ModalFooter>
</Modal></div></div>
);}
export default ResetPassword;
