import React, {useState} from 'react';
import {Link, useHistory} from 'react-router-dom';
import './registro.css';
import swal from 'sweetalert';
import {Mutation} from '@apollo/client/react/components';
import {REGISTRAR_USUARIO_NEW} from '../graphql/mutations/usuario';
import {BUSCAR_POR_NOMBRE} from '../graphql/queries/usuario';
import config from '../../config';
import {useMutation, useQuery} from '@apollo/client';
import history from '../utils/history';


const RegistroUsuarios = () => {
    const [usuario, setFormulario] = useState(null);
    const [registrarUsuario] = useMutation(REGISTRAR_USUARIO_NEW, {
        onCompleted: (result => {
            
        if(result !=  null ){
            console.log(result.usuarios.registrarUsuario)
             var resp = result.usuarios.registrarUsuario;
            swal({title: resp.nombre, text: "Bien hecho!, inicia sesion",icon: "success", button: "Ok"})
            history.push('/Login')
        }else{
            return  swal({title: resp.nombre, text: "Intenta de nuevo", icon: "error", button: "Ok"})
        }
           

        })
    })

     const [userName, setuserName] = useState("")
    const {data,loadding,error} = useQuery(BUSCAR_POR_NOMBRE,{
        variables:{
            user:userName
        },
        onCompleted:(res =>{
            if(res.usuarios.obtenerPorUsuario == null){  
            console.log(res)
            }else{ 
            swal({title: "error", text: "Ese usuario ya existe",icon: "error", button: "Ok"})
           
            }
        })
    })


    const Registrar = () => {
        // if(usuario!= "") {
        //     return   swal({title: "Error", text: "Todos los campos son requeridos", icon: "error", button: "Ok"})       
        // }
        registrarUsuario({
            variables:{
                user:usuario
            }
        })

    }

    return (
        <div>

            <div className="container">
                <div className="card-Registro">
                    <div className="card-header">
                        <p id="registroTitle">Crear Cuenta</p>
                    </div>
                    <div className="card-body">
                        <form onSubmit={ e=>{
                            e.preventDefault()
                            Registrar()
                        }} id="myFormUsuarios">
                            <div className="form-group">
                                <label htmlFor="Nombre" id="labelPusition">Nombre</label>
                                <input required type="txt" className="form-control validate" id="InputsFormRegistro" 
                                    onChange={
                                        e => {
                                            setFormulario({
                                                ...usuario,
                                                nombre: e.currentTarget.value
                                            })

                                        }
                                    }
                                    name="Nombre"
                                    placeholder="Nombre Completo"/>

                            </div>
                        <div className="form-group">
                            <label htmlFor="Usuario" id="labelPusition">Usuario</label>
                            <input type="txt" className="form-control validate" id="InputsFormRegistro" required
                                onChange={
                                    e => {
                                        setFormulario({
                                            ...usuario,
                                            user: e.currentTarget.value
                                        })
                                        setuserName( e.currentTarget.value)

                                    }
                                }
                                name="Usuario"
                                minlength="6"
                               
                                placeholder="Usuario"/>
                        </div>

                        <div className="form-group">
                            <label htmlFor="correo" id="labelPusition">Correo</label>
                            <input type="email" className="form-control validate" id="InputsFormRegistro" required
                                onChange={
                                    e => {
                                        setFormulario({
                                            ...usuario,
                                            correo: e.currentTarget.value
                                        })

                                    }
                                }
                                name="Correo"
                                placeholder="Email"/>
                        </div>

                    <div className="form-group">
                        <label htmlFor="Contraseña" id="labelPusition">Contraseña</label>
                        <input type="password" className="form-control validate" id="InputsFormRegistro" required
                            onChange={
                                e => {
                                    setFormulario({
                                        ...usuario,
                                        contra: e.currentTarget.value
                                    })

                                }
                            }
                            name="Contraseña"
                            minlength="8"
                            maxLength="12"
                            placeholder="Contraseña"/>
                    </div>

                {/* <div className="form-group">
                                <label htmlFor="ContraseñaConfirm" id="labelPusition">Confirmar
                                </label>
                                <input type="password" className="form-control" id="InputsFormRegistro" required
                                    onChange={  e => {
                                        setFormulario({...UsuarioInput, Contra: e.currentTarget.value})
    
                                       }
                                    }
                                    name="ContraseñaConfirm"
                                    placeholder="Confirmar Contraseña"
                                />
                            </div> */}

                <div className="form-group">
                    <label htmlFor="Direccion" id="labelPusition">Direccion</label>
                    <input type="txt" className="form-control validate" id="InputsFormRegistro" required
                        onChange={
                            e => {
                                setFormulario({
                                    ...usuario,
                                    direccion: e.currentTarget.value
                                })

                            }
                        }
                        name="Dirección"
                        minlength="10"
                        
                        placeholder="Dirección"/>
                </div>

            <div className="form-group">
                <label htmlFor="" id="labelPusition">Telefono</label>
                <input type="number" className="form-control validate" id="InputsFormRegistro" required
                    onChange={
                        e => {
                            setFormulario({
                                ...usuario,
                                telefono: e.currentTarget.value
                            })

                        }
                    }
                    name="Teléfono"
                    minlength="10"
                    placeholder="Teléfono"/>
            </div>

        <br/>
        <div className="form-group">
            <select name="Cliente" className="form-control validate" id="InputsFormRegistro" required
                onChange={
                    e => {
                        setFormulario({
                            ...usuario,
                            rol: e.currentTarget.value
                        })

                    }
            }>
                <option selected>Rol</option>
                <option name="Vendedor" value="Vendedor">Vendedor</option>
                <option name="Cliente" value="Cliente">Cliente</option>

            </select>
        </div>

        <div className="form-group">
            <button type="submit" className="btn btn-success" id="botonVerde"
            //     onClick={
            //         (e) => {
                        
            //             Registrar()
            //         }
            // }
            >
                Aceptar
            </button>
        </div>
    </form>


</div>
<hr/></div></div></div>
    );
}
//


export default RegistroUsuarios;
