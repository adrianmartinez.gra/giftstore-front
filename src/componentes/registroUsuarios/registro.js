import React, { useState } from 'react';

const Registro =() => {

    const [formulario, setFormulario] = useState(null)


    const Registrar = (e) =>{
        e.preventDefault();
        console.log(formulario)
    }

    return (
        <div>
            <form id="myFormUsuarios">
                <div className="form-group">
                    <label htmlFor="Nombre" id="labelPusition">Nombre</label>
                    <input type="txt" className="form-control" id="InputsFormRegistro" required
                        onChange={
                            e => {
                                setFormulario({...formulario,Nombre: e.currentTarget.value
                                })

                            }
                        }
                        name="Nombre"
                        placeholder="Nombre Completo" />

                </div>
                <div className="form-group">
                    <label htmlFor="Usuario" id="labelPusition">Usuario</label>
                    <input type="usuario" className="form-control" id="InputsFormRegistro" required
                         onChange={  e => {
                            setFormulario({...formulario, Usuario: e.currentTarget.value})

                           }
                        }
                        name="Usuario"
                        placeholder="Usuario"
                    />
                </div>

                <div className="form-group">
                    <button type="button" className="btn btn-success" id="botonVerde"
                        onClick={(e) => {
                            Registrar(e)
                        }
                        }>
                        Aceptar
                            </button>
                </div>
            </form>

        </div>
    )
}

export default Registro;
