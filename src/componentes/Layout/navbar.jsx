import React, {Component, useState} from 'react';
import './menuNavBar.css';
import ModalLogin from '../modalLogin/modalLogin';
import {Modal} from 'reactstrap'
import {Link, Route} from 'react-router-dom';
import history from '../utils/history'
import InicioCatalogo from '../inicioCatalogo/inicioCatalogo';

// iconos
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faUserCircle, faCartArrowDown, faSignOutAlt, faAddressCard} from '@fortawesome/free-solid-svg-icons';
import { useEffect } from 'react';
import { useQuery } from '@apollo/client';

import {DATOS_USUARRIO_BYID} from '../graphql/queries/usuario';



const Navegacion = (props) => {

 const [user, setUser] = useState(null)
    useEffect(() => {
        //fetchData();
        if(localStorage != null )
        console.log(user)
        return () => {

        }
    },[] )

   
  
    const {refetch:fetchData}  = useQuery(DATOS_USUARRIO_BYID,{
        variables:{
            id: localStorage.getItem('datosUsuario')
        },
        onCompleted:(res =>{
            setUser(res.usuarios.obtenerById)
        })
    })

      

    const setEstado = () => {//salir
        setUser(localStorage.removeItem('datosUsuario'))
          localStorage.removeItem('usuarioLogeado');
        history.push('/GiftStore')
    }


    const CerrarSesion = (e) => {
        localStorage.removeItem('datosUsuario')

    }

    return (

        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div>
                <Link to="/GiftStore">
                    <a className="navbar-brand" href="#">
                        <p className="GiftStoreInicio">GiftStore
                        </p>
                    </a>
                </Link>
            </div>

            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarColor02">
                <ul className="navbar-nav mr-auto" id="menuNav">

                    <li className="nav-item">
                        <a className="nav-link" href="#"></a>
                        <div id="hoverOptionUser">
                            <FontAwesomeIcon icon={faUserCircle}
                                id="userCircle"/>
                            <div> {
                                user ? "" : <Link to="/Login">
                                    <p id="inicio">inicio</p>
                                </Link>
                                  } 
                            </div>
                                  

                            <ul>
                                <a href="#">
                                    <div id="tamañoletrausuario">
                                        {
                                       user ? <div id=""> {user.user}</div> : ""
                                  
                                    } </div>
                                </a>

                                <ul>
                                    <div id="content-Option">
                                        <Link to={
                                            {
                                                pathname: "/MiPerfil",
                                                state: {
                                                    id: props.id
                                                }
                                            }
                                        }>
                                            <a href="#">
                                                <p id="efechover">
                                                    <FontAwesomeIcon icon={faAddressCard}
                                                        id="PerfilIcon"/>
                                                    Mi Perfil
                                                </p>
                                            </a>
                                        </Link><hr/>
                                        <Link to="/Carrito-Compras">
                                            <a href="#">
                                                <p id="efechover">
                                                    <FontAwesomeIcon icon={faCartArrowDown}
                                                        id="CarritoIcon"/>
                                                    Carrito
                                                </p>
                                            </a>
                                        </Link>
                                        <hr/>
                                        <a href="#" id="closeSesion"
                                            onClick={
                                                e => CerrarSesion(e)
                                        }>
                                            <p id="efechover">
                                                <FontAwesomeIcon icon={faSignOutAlt}
                                                    id="exitIcon"
                                                    onClick={
                                                        () => setEstado()
                                                    }/>
                                                Salir</p>
                                        </a>
                                    </div>

                                </ul>
                                {/* </li> */} </ul>

                        </div>

                    </li>
                </ul>
            </div>


        </nav>
    );


}

export default Navegacion;
