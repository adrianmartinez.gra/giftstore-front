import React, {Component, useState} from 'react';
import './modal.css';
import {Link} from 'react-router-dom';
import swal from 'sweetalert';
import {Mutation} from '@apollo/client/react/components';
import history from '../utils/history';
import {useLazyQuery, useMutation, useQuery} from '@apollo/client';
import {Login_Usuario} from '../graphql/queries/usuario';
import MiPerfil from '../miPerfil/miPerfil';
import {faGofore} from '@fortawesome/free-brands-svg-icons';
import {Navbar} from 'reactstrap';
import Navegacion from '../Layout/navbar'


const ModalLogin = (props) => {
    console.log(props)
    let datosUser = []

    const [datosUsuario, setDatosUsuario] = useState(null)


    const [getUser, {
            loading,
            data
        }
    ] = useLazyQuery(Login_Usuario, {
        onCompleted: (result => {
                if (result != null) {
                        try { // localStorage.setItem('usuarioLogeado', JSON.stringify(result.usuarios.loginUser.user))
                            console.log(result)
                            console.log(result.usuarios.loginUser.id)
                            datosUser.push(result.usuarios.loginUser);
                            swal({title: datosUser[0].user, text: "Bienvenido!", icon: "success", button: "Aceptar!"});

                            localStorage.setItem('datosUsuario', JSON.stringify(datosUser[0].id))
                            // history.push('/Nav')

                            history.push('/GiftStore')
                        } catch {
                            swal(
                                {title: user.value, text: "No se encontro el usuario!", icon: "error", button: "Aceptar!"}
                            );
                        }} else {
                        swal({title: user.value, text: "No se encontro el usuario!", icon: "error", button: "Aceptar!"});
                    }


                }
            )}
    )

    const limpiarFormulario = () => {
        document.getElementById("myForm").reset();
    }

    const [user, setUser] = useState(null)
    const [contra, setContra] = useState(null)

    const Login = () => {

        getUser({

            variables: {
                user: user,
                contra: contra
            }
        })
    }

    return (

        <div>
            <div className="container">
                <div className="card-Login">
                    <div className="card-header">
                        <p id="Login">LOGIN</p>
                    </div>
                    <div className="card-body">

                        <form id="myForm">
                            <div className="form-group">
                                <input type="text" className="form-control validate"

                                    onChange={
                                        (e) => {
                                            setUser(
                                                // ...user,
                                                    e.currentTarget.value
                                            )
                                        }
                                    }
                                    id="tamañoInputLogin"
                                    name="user"
                                    placeholder="Ingrese su usuario"/>

                            </div>
                        <div className="form-group">
                            <input type="password" className="form-control validate"
                                onChange={
                                    (e) => {
                                        setContra(
                                            // ...contra,
                                                e.currentTarget.value
                                        )
                                    }
                                }
                                id="tamañoInputLogin"
                                name="pass"
                                placeholder="Password"/>
                        </div>

                    <div className="form-group">
                        <button type="button" className="btn btn-primary"
                            onClick={
                                async e => {
                                    e.preventDefault()
                                    await Login();
                                }
                            }
                            id="botonAzul">
                            Iniciar Sesion

                        </button>
                    </div>
                </form>

                <div className="form-group">
                    <Link to="/Registro">
                        <button type="button" className="btn btn-success" id="botonVerde">
                            Crear Cuenta</button>
                    </Link>
                </div>

                <div className="form-group">
                    <Link to="/RestablecerContraseña">
                        <a href="#" id="enlaceRecuperarContra">
                            Olvide mi contraseña?
                        </a>
                    </Link>
                </div>
            </div>
            <hr/>
        </div>

    </div>
</div>


    );}
    export default ModalLogin;
