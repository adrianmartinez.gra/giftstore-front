import React, {Component} from 'react';
import './catalogoStyles.css';
import {Link, Route} from 'react-router-dom';
import {
    Card,
    CardText,
    CardBody,
    ButtonToggle,
    CardTitle
} from 'reactstrap';
import registroUsuarios from '../registroUsuarios/registroUsuarios';
import Navegacion from '../Layout/navbar';
import ListaProductos from './listaProductos';

class InicioCatalogo extends Component {

    constructor(props) {
        super(props);
    

    this.state = {
     productos:[
    {Categoria:"Calzado",Nombre: "adidas", Descripcion: "tenias adidas ", Precio: 510},
    {Categoria:"Deportes",Nombre: "playera", Descripcion: "playera hunder ", Precio: 650},
    {Categoria:"Tecnologia",Nombre: "Laptop", Descripcion: "lenovvo max turbo ", Precio: 800},
    {Categoria:"Belleza",Nombre: "Collar", Descripcion: "Collar de plata ", Precio: 150},
    {Categoria:"Regalos",Nombre: "adidas", Descripcion: "tenias adidas ", Precio: 250},
 
    
     ]
   }

}
      
    
    handlerValuesChange = (e) =>{
        e.preventDefault();
        this.setState({Ropa: e.target.value })

    }

    handlerPrecio = (e) =>{
        this.setState({
            [e.target.name]:e.target.value +0
        })
        this.SendPrecioConsole();
    }

    SendPrecioConsole = () =>{
        console.log(this.state.precio)
    }

    render() {

        return (
            <div>
                <div className="container">
                    <div >
                        <select id="filtroSelect" name="Ropa" 
                        onChange={this.handlerValuesChange}
                         className="form-control" id="selectBuscar"  type="text"
                         >
                            <option selected>Categorias</option>
                            <option  name="Perfumes" value="Perfumes">Perfumes</option>
                            <option  name="Ropa" value="Ropa">Ropa</option>
                            <option value="Joyeria" name="Joyeria">Joyeria</option>
                            <option value="Regalos" name="Regalos">Regalos de oferta</option>
                            <option value="Calzado" name="Calzado">Calzado</option>
                        </select>


                        <input type="number" className="form-control" id="inputBuscar"
                        onChange={this.handlerPrecio} name="precio" placeholder="Precio $200 MXN"/>

                    </div>
                    <br/> <br/>
                    <div className="clearFix"></div>

                    <div>
                        
                          {
                              this.state.productos.map(pro => {
                                  return (
                                  <ListaProductos pro={pro} key={pro}  />

                                  )

                              })
                          }                 
                    </div>

                </div>


            </div>


        );
    }

}
export default InicioCatalogo;
