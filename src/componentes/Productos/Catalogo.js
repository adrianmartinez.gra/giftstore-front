import React, { useState } from 'react'
import { Card, CardText, CardHeader, CardBody, ButtonToggle, CardTitle, CardFooter } from 'reactstrap';
import './Catalogo.css'
import { OBTENER_PRODUCTOS_POR_CATEGORIA, OBTENER_PRODUCTO_POR_NOMBRE } from '../graphql/queries/productos';
import { CAMBIAR_FAVORITO } from '../graphql/mutations/productos';
import { useQuery, useMutation, useLazyQuery } from '@apollo/client';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import history from '../utils/history';
import { Mutation } from '@apollo/client/react/components';


const Catalogo = () => {
    const [Categoria, setCategoria] = useState("")
    const [elemento, setelemento] = useState({})


    let categories = [
        { id: 1, nombre: "Todos" },
        { id: 2, nombre: "Electronicos" },
        { id: 3, nombre: "Hogar" },
        { id: 4, nombre: "Ropa y accesorios" }

    ]
    const [Productos, setProductos] = useState([])
    // const [ProductoBuscado, setProductoBuscado] = useState(null)

    const [Term, setTerm] = useState("")
    let { error, loading } = useQuery(OBTENER_PRODUCTO_POR_NOMBRE,
        {
            variables: { nombre: Term },
            onCompleted: (async res => {
                await setProductos(res.productos.obtenerByName)
                console.log(res.productos.obtenerByName);
                ref()
            }),
        })

    const Categorias = (e) => {
        if (e.currentTarget.value === "Todos") {
            setCategoria("")

        } else {
            setCategoria(e.currentTarget.value)
        }
    }

    const  { data, refetch: refetchProducts    } = useQuery(OBTENER_PRODUCTOS_POR_CATEGORIA,
        {
            variables: { categoria: Categoria },
            onCompleted: ( async res => {

                await  setProductos(res.productos.obtenerByCategory);
                console.log( res);
                // cambiarEstiloIconFav(respuesta);
                //cambiarEstiloIconFav()
                
                ref()
                return
            }),
        })

        const ref = () =>{
            refetchProducts()
        }
        
    const cambiarEstiloIconFav = (e) => {
        if (e != null) {
            if (elemento.isFavorit != false) {
                let heart = document.getElementById('iconFavoritos')
                heart.style.color = '#F51F32';
            } else {
                let heart2 = document.getElementById('iconFavoritos')
                heart2.style.color = "rgb(199, 194, 194)";
            }
        }

        // }


    }

    const [favorito, setfavorito] = useState(false)
    const toggle = () => setfavorito(!favorito);

    const [updateFav] = useMutation(CAMBIAR_FAVORITO, {
      
        onCompleted: (async res => {
            console.log(res.productos.cambiarEstusFav)
          await  refetchProducts()

        })
    })
    const favoritoSAVE = (e) => {
        toggle(toggle)
        //cambiarEstiloIconFav(e)
        updateFav({
            variables: {
                id: e.id,
                favorit: favorito
            }
        }) 
        refetchProducts()
    }
    const openCard = (e) => {
        history.push(`/Detalle/clave=${e.id}`)
    }
    if (error) {
        return <p>`Error... ${error.message}`</p>
    }

    if (Productos != null) {
        return (
            <>
                <br /><br />
                <div className="container">

                    <div className="clearFix"></div>

                    <div id="titulo"> Encuentra lo que buscas</div><br />
                    <div id="filtrosPrincipales">
                        <input type="text" id="InputBuscarProducto" className="form-control" placeholder="Buscar...."
                            onChange={(e) => {
                                e.preventDefault()
                                setTerm(e.currentTarget.value)
                            }} ></input>
                        <div>
                            <select className="form-control " id="options" onChange={e => { Categorias(e) }} >
                                {categories.map(e => (
                                    <option key={e.id} value={e.nombre}>
                                        {e.nombre}
                                    </option>
                                ))}
                            </select>
                        </div>
                    </div>


                    {Productos.map( e => {
                        if (e.isActive === true) {
                            return (
                                <div key={e.id}>
                                    <Card >
                                        {/* <Link id="cardStileText" to={`/Detalle/clave=${e.id}`}  > */}
                                        <CardHeader id="Categoria">{e.categoria}

                                            { e.isFavorit === true ?
                                                <FontAwesomeIcon icon={faHeart} id="iconFavoritos" style={{color: "red"}}
                                                    onClick={( ) =>
                                                        
                                                        favoritoSAVE(e)} /> :
                                                <FontAwesomeIcon icon={faHeart} id="iconFavoritos" style={{color: "gray"}}
                                                    onClick={() =>
                                                        favoritoSAVE(e)} />}


                                            {/* {e.isFavorit === true ? <p > Rojo </p> : <p> Gris</p>} */}

                                        </CardHeader>
                                        <CardBody id="CardText" onClick={() => openCard(e)}>
                                            <CardText> FOTO </CardText>
                                        </CardBody>
                                        {/* </Link> */}
                                        <div className="clearfix"></div>
                                        <CardFooter id="footerCard" onClick={() => openCard(e)}>
                                            <div id="precioColor">
                                                $ {e.precio}
                                            </div>
                                            <p id="descripcionDeProductoHover">{e.descripcion}</p>
                                        </CardFooter>

                                    </Card>

                                    <div >

                                    </div>

                                </div>
                            )
                        } else {
                            return null
                        }
                    })}
                </div>
            </>
        )

    }

    ///Buscar producto

    // return (

    //     <>
    //         <div>
    //             <select className="options" onChange={e => { Categorias(e) }} >
    //                 {categories.map(e => (
    //                     <option key={e.id} value={e.nombre}>
    //                         {e.nombre}
    //                     </option>
    //                 ))}
    //             </select>
    //         </div>


    //         <>
    //             <div id="titulo"> <br />Productos Disponibles</div>

    //             {ProductoBuscado ? ProductoBuscado.map(e => {
    //                 if (e.isActive === true) {
    //                     return (
    //                         <>
    //                             <div className="container">
    //                                 <div className="clearFix"></div>

    //                                 <Card>
    //                                     <Link to={`/Detalle/clave=${e.id}`}  >
    //                                         <CardBody>
    //                                             <CardTitle>{e.nombre}</CardTitle>
    //                                             <CardText>{e.descripcion}</CardText>
    //                                             {/* <ButtonToggle color="warning">agregar al carrito  </ButtonToggle> */}
    //                                         </CardBody>
    //                                     </Link>

    //                                     <CardFooter>
    //                                         <ButtonToggle color="success">Comprar $ {e.precio} </ButtonToggle>
    //                                         <CardText id="Categoria">{e.categoria} </CardText>
    //                                     </CardFooter>
    //                                 </Card>
    //                             </div>
    //                         </>
    //                     )

    //                 } else return null
    //             }) : null}

    //         </>


    //     </>
    // )
}
export default Catalogo
