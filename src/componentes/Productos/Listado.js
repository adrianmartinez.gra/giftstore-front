import { Table, Button, Spinner, Label, FormGroup, Form, Input, Modal, ModalHeader, ModalBody, ModalFooter, } from 'reactstrap';

import React, { useState } from 'react'
import './productos.css'
import { useMutation, useQuery } from '@apollo/client'
import { OBTENER_PRODUCTOS } from '../graphql/queries/productos'
import Actualizarproducto from './Actualizarproducto';
import { Mutation } from '@apollo/client/react/components'
import { AGREGAR_PRODUCTO, CAMBIAR_ESTATUS } from '../graphql/mutations/productos';
import moment from 'moment'
import { faUser, faUserEdit, faPlusSquare, faArrowAltCircleUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom'
import swal from 'sweetalert';




function Listado() {
  console.log("Mounted");
  const [show, setShow] = useState(false);//muestra modal
  const [element, setElement] = useState({})//elemento que quiero pasar
  const closeModalHandler = () => setShow(false);//metodo que cierra el modal
  // moment.locale('es')
  const [buscador, setbuscador] = useState("")
  const [form, setform] = useState(null)
  const { data, error, loading, refetch: refetchProducts } = useQuery(OBTENER_PRODUCTOS)
  const [IsActive, setIsActive] = useState(false)
  const [OpenModal, setModal] = useState(false)
  
  const [Registrarproducto] = useMutation(AGREGAR_PRODUCTO,

    {
      onCompleted: (res => {
        swal({
          title: "¡¡ Guardado con exito!!", icon: "success"
        })
      })
    }
  )

  let categories = [
    { id: 1, nombre: "Ropa y accesorios" },
    { id: 2, nombre: "Electronicos" },
    { id: 3, nombre: "Deportes" },
    { id: 4, nombre: "Videojuegos" },
    { id: 5, nombre: "Hogar" }
  ]
  if (error) {
    return <p>`Error... ${error.message}`</p>
  }
  if (loading) {
    return (
      <div id='spinner'>
        <Spinner color="primary" />
        <Spinner color="secondary" />
        <Spinner color="success" />
        <br />
        <div>Cargando¡Por favor Espere! </div>
      </div>
    )
  }
  const toggle = () =>{
    console.log(!OpenModal);
    setModal(!OpenModal);
  }
  


  async function RegistrarNuevoProducto() {
    await Registrarproducto({ variables: { producto: form } })
    setform('')
    refetchProducts()
    toggle()
  }


  const CambiarEstatus = (mutate) => {
    mutate()
    refetchProducts()

  }

  const FiltroBusqueda = data.productos.obtenerTodos.filter((prod) => {
    if (prod) {
      return (
        prod.nombre.toLowerCase().includes(buscador.toLowerCase()) || prod.categoria.toLowerCase().includes(buscador.toLowerCase())
      )
    } else {
      return (
        console.log("nada"),
        <p>No se encontró nada</p>)
    }

  });

  return (
    <>
      <div className="test"> {/* Este div contiene el modal */}



        {show ? <div onClick={closeModalHandler} className="back-drop"></div> : null}
        {/* Aqui valido si show es true mando a llamar al closemodal para que lo cierre sino devuelvo nulo */}
        <Actualizarproducto show={show} close={closeModalHandler} element={element} />
        {/* Aqui mando a llamar al modal y le paso las props */}
      </div>
      <div className="SwitchContainer">
        Mostrar Inactivos
          <label className="switch">
          <input type="checkbox" defaultValue={IsActive} onChange={e => { setIsActive(!IsActive) }} />
          <span className="slider round"></span>
        </label>
      </div>
      <div>
      </div>

      {/* Tabla de activos */}
      {!IsActive ?
        <>
          <div className="container ">

            <Modal backdrop="static" id="Modal_AP" isOpen={OpenModal} toggle={toggle} >
              {/* <ModalHeader toggle={toggle}> Agregar Producto</ModalHeader> */}
              <ModalBody>

                <Form id='AgregarProductosForm' >
                  <FormGroup id="form-Group">
                    <Label className="label">Nombre: </Label>
                    <Input required autoFocus={true} placeholder="Nuevo nombre" onChange={event =>
                      setform({ ...form, nombre: event.currentTarget.value })} />
                  </FormGroup>

                  <FormGroup id="form-Group">
                    <Label className="label">Precio: </Label>
                    <Input type='number' required placeholder="Nuevo precio" onChange={event =>
                      setform({ ...form, precio: event.currentTarget.value })} />
                  </FormGroup>

                  <FormGroup id="form-Group">
                    <Label className="label">Descripcion: </Label>
                    <Input type="textarea" required placeholder="Nueva descripcion" onChange={event =>
                      setform({ ...form, descripcion: event.currentTarget.value })} />
                  </FormGroup>

                  <FormGroup id="form-Group">
                    <Label className="label">Cantidad: </Label>
                    <Input type='number' required placeholder="Nueva cantidad" onChange={event =>
                      setform({ ...form, cantidad: event.currentTarget.value })} />
                  </FormGroup>

                  <FormGroup id="form-Group">
                    <Label className="label">Categoria: </Label>
                    <Input required type="select" name="select" id="exampleSelect" onChange={event =>
                      setform({ ...form, categoria: event.currentTarget.value })}>
                      {categories.map((e) => (
                        <option key={e.id} value={e.nombre}>
                          {e.nombre}
                        </option>
                      ))}
                    </Input>

                  </FormGroup>

                  {/* <FormGroup>
          <Label >Fotos: </Label>
          <Input type='file' required placeholder="Agrega fotos" onChange={async e => {
            await handleChange(e)


          }} />
        </FormGroup> */}
                  {/* setear form, incluyendo url de la imagen */}
                <hr></hr>
                <div id="footerButtonsModal">
                    <Button color="info" id="buttonGuardarModal" onClick={ async () => { await RegistrarNuevoProducto() }} >Guardar </Button>{""}
                  <Button color="secondary" onClick={toggle}>Cancel</Button>
                </div>
                
                </Form>
              </ModalBody>
              <ModalFooter>
              </ModalFooter>
            </Modal>

            <div id="ProductosActivosTitulo" >Mis Productos</div>
            <div id="btnAddProductoContainer">
               <Button type="btn" id="btnAgregar" className="btn btn-success" onClick={toggle} >
              Agregar Producto
                    <FontAwesomeIcon icon={faPlusSquare} id="addProdIcon" />
            </Button>
            <input type="text" className="form-control" id="filtro_tabla" autoFocus  placeholder="Busca un nombre o categoria" onChange={e => setbuscador(e.currentTarget.value)} />
            
            </div>
           
            
            <Table id='tabla' striped hover bordered responsive>
              <thead >
                <tr>
                  <th>Descripcion</th>
                  <th>Nombre</th>
                  <th>Precio</th>
                  <th>Cantidad</th>
                  <th>Categoria</th>
                  <th>Fecha de agregado</th>
                  {/* <th>Estado</th> */}
                  <th>Acciones</th>
                </tr>
              </thead>
              {FiltroBusqueda ? FiltroBusqueda.map(e => {
                if (e.isActive === true) {

                  return (<>
                    <tbody key={e.id}>
                      <tr >
                        <th scope="row">{e.descripcion}</th>
                        <td >{e.nombre} </td>
                        <td>{e.precio} </td>
                        <td>{e.cantidad} </td>
                        <td>{e.categoria} </td>
                        <td>{moment(e.dateCreated).format('LL')}</td>
                        {/* <td>{e.isActive ? <p>Activo</p> : <p>Inactivo</p>}</td> */}
                        <td>
                          <div className='modalbuttons'>
                            <Button onClick={() => {//boton que manda a llamar al modal
                              return (setShow(true),//cambio de estado para que se muestre
                                setElement(e)//seteo el elemento que quiero enviar
                              )
                            }} id='editar' outline color='warning'>Editar</Button>{""}

                            <Mutation mutation={CAMBIAR_ESTATUS}
                              variables={{ id: e.id }}>
                              {
                                (mutate) => {
                                  return (
                                    <Button onClick={() => CambiarEstatus(mutate)} outline color="secondary">Inactivar</Button>
                                  )
                                }
                              }
                            </Mutation>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </>
                  )
                } else {
                  return null
                }
              }
              ) : <div>
                  <p>No hay productos Activos en este momento</p>
                  {/* <div>Inactivados: {data.productos.obtenerTodos.length}</div> */}
                </div>
              }
            </Table>
          </div>
        </> : // Cierre de Tabla de Productos Activos
        //
        //Productos inactivos
        <>
          <div className="container">
            <div id="ProductosActivosTitulo"> Productos Inactivos</div>

            <Table id='tabla' hover bordered responsive>
              <thead>
                <tr>
                  <th>Descripcion</th>
                  <th>Nombre</th>
                  <th>Precio</th>
                  <th>Cantidad</th>
                  <th>Categoria</th>
                  <th>Fecha de agregado</th>
                  {/* <th>Estado</th> */}
                  <th>Acciones</th>
                </tr>
              </thead>
              {FiltroBusqueda ? FiltroBusqueda.map(e => {
                if (e.isActive === false) {
                  return (<>
                    <tbody key={e.id}>
                      <tr >
                        <th scope="row">{e.descripcion}</th>
                        <td >{e.nombre} </td>
                        <td>{e.precio} </td>
                        <td>{e.cantidad} </td>
                        <td>{e.categoria} </td>
                        <td>{moment(e.dateCreated).format('LL')}</td>
                        {/* <td>{e.isActive ? <p>Activo</p> : <p>Inactivo</p>}</td> */}
                        <td>
                          <div className='modalbuttons'>
                            <Button onClick={() => {//boton que manda a llamar al modal
                              return (setShow(true),//cambio de estado para que se muestre
                                setElement(e)//seteo el elemento que quiero enviar
                              )
                            }} id='editar' outline color='warning'>Editar</Button>{""}

                            <Mutation mutation={CAMBIAR_ESTATUS}
                              variables={{ id: e.id }}>
                              {
                                (mutate) => {
                                  return (
                                    <Button onClick={() => CambiarEstatus(mutate)} outline color="secondary">Ativar</Button>
                                  )
                                }
                              }
                            </Mutation>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </>
                  )
                } else {
                  return null
                }
              }
              ) : <div>
                  <p>No hay productos Inactivos en este momento</p>
                  {/* <div>Inactivados: {data.productos.obtenerTodos.length}</div> */}
                </div>
              }
            </Table>
          </div>
        </>

      }


    </>

  )
}

export default Listado
