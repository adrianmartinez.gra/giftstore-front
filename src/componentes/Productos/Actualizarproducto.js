import { Mutation } from '@apollo/client/react/components';
//import { formatMs } from '@material-ui/core';
import React, { useState } from 'react'
import { Label, FormGroup, Form, Input, Button } from 'reactstrap';
import swal from 'sweetalert';
import { AGREGAR_PRODUCTO } from '../graphql/mutations/productos';
import './productos.css'
//import { OBTENER_PRODUCTOS } from '../../graphql/queries/productos'

const Actualizarproducto = ({ show, close, element }) => { // Aqui recibo las props que puse cunado lo mande a llamar
    const [form, setform] = useState("")

    let categories = [
        { id: 1, nombre: "Ropa y accesorios" },
        { id: 2, nombre: "Electronicos" },
        { id: 3, nombre: "Deportes" },
        { id: 4, nombre: "Videojuegos" },
        { id: 5, nombre: "Hogar" }
    ]

    const Actualizar = (mutate) => {
        mutate();
        if (mutate) {
            swal({ text: 'Producto agregado exitosamente', icon: 'success', button: 'Aceptar' })
            console.log(form);
            setform('')
        } else {
            swal({ text: 'Una entrada faltante', icon: 'error', button: 'Aceptar' })
        }
    }

    return (
        <>
            <div className="modal-wrapper"
                style={{
                    transform: show ? 'translateY(0vh)' : 'translateY(-100vh)',
                    // Estos son efectos del modal segun si esta abierto es decir show es igual a true
                    opacity: show ? '1' : '0'
                }}>
                <div id="updateProductoheader">
                    <p id="titleUpdateProduct">Actualizar producto</p>
                    <span onClick={close} className="close-modal-btn">x</span> {/* El boton de X solo lo cierra */}
                </div>
                <div className="modal-content">
                    <div className="modal-body">
                        <Mutation mutation={AGREGAR_PRODUCTO}
                            variables={{ producto: form }}>

                            {
                                (mutate) => {
                                    return (
                                        <Form onSubmit={async e => {
                                            await e.preventDefault()
                                            Actualizar(mutate)
                                            close()
                                        }}>
                                            <FormGroup>
                                                <Label >Nombre: </Label>
                                                <Input required defaultValue={element.nombre} autoFocus={true} placeholder="Nuevo nombre" onChange={event =>
                                                    setform({ ...form, nombre: event.currentTarget.value, id: element.id })} />
                                            </FormGroup>

                                            <FormGroup>
                                                <Label >Precio: </Label>
                                                <Input type='number' required defaultValue={element.precio} placeholder="Nuevo precio" onChange={event =>
                                                    setform({ ...form, precio: event.currentTarget.value, id: element.id })} />
                                            </FormGroup>

                                            <FormGroup>
                                                <Label >Descripcion: </Label>
                                                <Input type="textarea" required defaultValue={element.descripcion} placeholder="Nueva descripcion" onChange={event =>
                                                    setform({ ...form, descripcion: event.currentTarget.value, id: element.id })} />
                                            </FormGroup>

                                            <FormGroup>
                                                <Label >Cantidad: </Label>
                                                <Input type='number' required defaultValue={element.cantidad} placeholder="Nueva cantidad" onChange={event =>
                                                    setform({ ...form, cantidad: event.currentTarget.value, id: element.id })} />
                                            </FormGroup>

                                            <FormGroup>
                                                <Label >Categoria: </Label>
                                                <Input required type="select" name="select" id="exampleSelect" onChange={event =>
                                                    setform({ ...form, categoria: event.currentTarget.value, id: element.id })}>
                                                    {categories.map((e) => (
                                                        <option key={e.id} value={e.nombre}>
                                                            {e.nombre}
                                                        </option>
                                                    ))}
                                                </Input>

                                            </FormGroup>
                                            <Button color="info"  >Actualizar </Button>{""}
                                        </Form>
                                    )
                                }
                            }
                        </Mutation>

                    </div>
                    <div className="modal-footer">
                        <Button onClick={close} color="danger">Cerrar</Button>{/* Cierra modal */}
                    </div>
                </div>
            </div>
        </>
    )
};

export default Actualizarproducto
