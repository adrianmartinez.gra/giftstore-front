import React, { useState, useEffect } from 'react'
import "./productos.css";
import { Card, CardText, CardBody, CardTitle, Button, CardSubtitle, Spinner, CardFooter, ButtonToggle } from 'reactstrap';
import { useQuery } from '@apollo/client';
import { OBTENER_PRODUCTO_POR_ID } from '../graphql/queries/productos';

const Detalle = (props) => {
    const [producto, setproducto] = useState({})
    const ID = props.match.params.id;
    console.log(ID);
    //const IdUser;
    //08d8721b-16db-8b5b-8189-a5da5ae87d23
    useEffect(() => {
        console.log(ID);
        console.log(props);
    }, [ID, props])

    let { error, loading } = useQuery(OBTENER_PRODUCTO_POR_ID,
        {
            variables: { id: ID },
            onCompleted: (result => {
                setproducto(result.productos.obtenerById)
            }),
        })

        //IdUser=result.productos.obtenerById.id

    if (loading) {
        return (<div id='spinner'>
            <Spinner color="primary" />
            <Spinner color="secondary" />
            <Spinner color="success" />
            <br />
            <div>
                Cargando ¡Por favor Espere!
            </div>
        </div>)
    }
    if (error) {
        return <p><br />`Error... ${error.message}`</p>
    }
    return (
        <>
            <br /><br />
            <div className="CardContainer">

                <p id="titulo">Detalle de Producto: {producto.nombre}</p>


                <div id="carta">

                    <div className="row">

                        <div class="col">
                            <div id="ImagenDetalle">
                                IMAGEN
                                <div>
                                    <img alt="Not Found" />
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div id="container">
                                <div>
                                    <p id="titulo">{producto.nombre}</p>
                                </div>

                                <div id="Descripcion">
                                    <div>
                                        Descripcion:<p>{producto.descripcion}</p>
                                    </div>
                                </div>

                                <div>
                                    <p>Precio: $ {producto.precio}</p>
                                </div>

                                <div>
                                    {producto.isActive ?
                                        <div>
                                            <p>Estatus: Disponible</p>
                                            <div>
                                                Disponibles: {producto.cantidad}
                                            </div>
                                        </div> :
                                        <p className="disable">No Disponible</p>}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div id="btnopciones">
                    <ButtonToggle id="btnComprar" outline color="success">Comprar $ {producto.precio} </ButtonToggle>

                    <ButtonToggle id="btnAgregar"  outline color="warning">Agregar {producto.nombre} a carrito</ButtonToggle>
                </div>
            </div>
        </>
    )
}
export default Detalle;