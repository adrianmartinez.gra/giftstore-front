import React, { useState } from "react";
import swal from 'sweetalert'
import { Mutation } from "@apollo/client/react/components";
import './productos.css'
import { AGREGAR_PRODUCTO } from "../graphql/mutations/productos";
import history from "../utils/history";

const ProductosForm = () => {
    const [form, setform] = useState('')

    const GurdarProducto = (mutate) => {
        mutate();
        if (mutate) {
            swal({ text: 'Producto agregado exitosamente', icon: 'success', button: 'Aceptar' })
            //console.log(form);
            setform(null)
            document.getElementById('form').reset()
            return history.push('/Productos/Administracion')
            
        } else {
            swal({ text: 'Una entrada faltante', icon: 'error', button: 'Aceptar' })
        }
    }


    return (<Mutation mutation={AGREGAR_PRODUCTO}
        variables={{ producto: form }} >
        {(mutate) => {
            return (
                <div id='products'>
                    <div >
                        <div id="titulo">Agregar Nuevo Producto</div>
                        <form id="form" onSubmit={async e => {
                            GurdarProducto(mutate)
                            await e.preventDefault();

                        }}>
                            <div className="form-group">
                                <label>Nombre: </label>
                                <input required type="text" placeholder="Nombre de producto" autoComplete="off" name='Nombre' onChange={e =>
                                    setform({ ...form, nombre: e.currentTarget.value })} className="form-control" autoFocus={true} />
                            </div>

                            <div className="form-group">
                                <label>Precio: </label>
                                <input required type='number' placeholder="Precio de producto" name='Precio' onChange={e =>
                                    setform({ ...form, precio: e.currentTarget.value })} className="form-control" />
                            </div>

                            <div className="form-group">
                                <label>Descripcion:</label>
                                <textarea required id='desc' type='text' placeholder="Escribre una descripcion" name='Descripcion' onChange={e =>
                                    setform({ ...form, descripcion: e.currentTarget.value })} className="form-control" />
                            </div>

                            <div className="form-group">
                                <label>Cantidad en existencia:</label>
                                <input  id='exist' type='number' required placeholder="Cantidad" name='Cantidad' onChange={e =>
                                    setform({ ...form, cantidad: e.currentTarget.value })} className="form-control" />
                            </div>
                            <div className="form-group">
                                <label>Categoria:</label>
                                <select placeholder='Escogue una..' required id='opciones' className="form-control" onChange={e =>
                                    setform({ ...form, categoria: e.currentTarget.value })}>
                                    <option value="">Elige una opción</option>
                                    <option>Ropa y accesorios</option>
                                    <option>Hogar</option>
                                    <option>Electronicos</option>
                                </select>
                            </div>

                            {/* <div className="form-group">
                                            <label>Agrega una foto:</label>
                                            <input required type="file" className="form-control-file" onChange={e =>
                                                setform({ ...form, foto: e.currentTarget.value })} />
                                        </div> */}

                            <button className="btn btn-success btn-block">Guardar</button>
                        </form>
                    </div>
                </div>
            )
        }
        }
    </Mutation>
    )
};
export default ProductosForm