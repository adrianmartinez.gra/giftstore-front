import React, {lazy, Suspense} from 'react';

import {Route, withRouter, Switch, Redirect} from 'react-router-dom';

// importacion de componentes
import PublicLayout from './componentes/Layout/publicLayout';
import PrivateLayout from './componentes/Layout/privateLayout';
import ModalLogin from './componentes/modalLogin/modalLogin';
import RegistroUsuarios from './componentes/registroUsuarios/registroUsuarios';
import MiPerfil from './componentes/miPerfil/miPerfil';
import Navegacion from './componentes/Layout/navbar';
import { Modal } from 'reactstrap';
import InicioCatalogo from './componentes/inicioCatalogo/inicioCatalogo';
import Catalogo from './componentes/Productos/Catalogo';
import Listado from './componentes/Productos/Listado';
import ResetPassword from './componentes/recuperarContraeña/recuperarContraseña';
import Carrito from './componentes/carrito/carrito';
// import AgregarProducto from './componentes/miPerfil/agregarProducto';
import ProductosForm from './componentes/Productos/productos';
import Detalle from './componentes/Productos/detalle_producto';



const InicioRoute = lazy(() => import ('./componentes/inicioCatalogo/inicioCatalogo'));
const adminpage = lazy(() => import ('./componentes/Layout/privateLayout'));

const registroUsuarios = lazy(()  => import ('./componentes/registroUsuarios/registroUsuarios'));
const Login = lazy(()  => import ('./componentes/modalLogin/modalLogin'));


const Routes = () => {

    // const usuarioLogin = false;
    // if (! usuarioLogin) {

        return (

               <Switch>
            
                        <Route path='/' exact component={Catalogo} />
                       <Route path="/Login" component={ModalLogin}/>
                       <Route path="/Registro" component={RegistroUsuarios} />
                       <Route path="/GiftStore"  component={Catalogo} />
                       <Route path="/MiPerfil" exact component={MiPerfil} />
                       <Route path="/Nav" exact component={Navegacion} />
                       <Route path="/Carrito-Compras" component={Carrito} />
                       <Route path="/ListadoProductos" component={Listado} />
                       <Route path="/Detalle/clave=:id" component={Detalle}/>
                       <Route path="/AddProducto" component={ProductosForm}/>
                       <Route path="/RestablecerContraseña" component={ResetPassword} />
                       {/* <Route path="/RecuperarContraseña" component={ActualizarContraseña} /> */}

                       <Route  path="**" Redirect to="/GiftStore" component={Catalogo}/>

               </Switch>
        );
}
export default withRouter(Routes);
