/* eslint-disable no-undef */

import   firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/storage';

const dev = {
    apiUrl: 'https://localhost:44320/',
  };
  
  const test = {
    apiUrl: '',
  };
  
  const production = {
    apiUrl: '',
  };
  
  const config =
    process.env.REACT_APP_STAGE === 'test'
      ? test
      : process.env.REACT_APP_STAGE === 'production'
      ? production
      : dev;
  
  export default {
    ...config,
  };
  

  
      // Initialize Firebase
     var  firebaseConfig = {
            apiKey: "AIzaSyDcOa1dK3R1U3o5apfVcntoZ_prAS9HK-k",
            authDomain: "uploadfiles-425df.firebaseapp.com",
            databaseURL: "https://uploadfiles-425df.firebaseio.com",
            projectId: "uploadfiles-425df",
            storageBucket: "uploadfiles-425df.appspot.com",
            messagingSenderId: "163815096354",
            appId: "1:163815096354:web:1131467ea8a7c2eabd8a7b"
          }
      
 
  firebase.initializeApp(firebaseConfig);
  const projectStorage = firebase.storage();
  const projectFirestore = firebase.firestore();

  export {projectFirestore, projectStorage};