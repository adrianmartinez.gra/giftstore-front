import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Router } from 'react-router-dom';
import Routes from './Routes';
import history from './componentes/utils/history';
// import inicioCatalogo from './componentes/inicioCatalogo/inicioCatalogo';
import Navegacion from './componentes/Layout/navbar';


function App() {
  return (

      <Router history={history}>
        <Navegacion/>
        <Routes />

      </Router>


  );
}

export default App;
